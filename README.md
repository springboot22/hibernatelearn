# JPA

## CriteriaBuilder 安全查询创建工厂

创建**CriteriaQuery**，创建查询具体具体条件**Predicate** 等。

用于构建JPA安全查询.可以从EntityManager 或 EntityManagerFactory类中获得CriteriaBuilder。 

```java
CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
```

### CriteriaQuery 安全查询主语句

- 它通过调用 CriteriaBuilder, createQuery 或CriteriaBuilder.createTupleQuery 获得。
- CriteriaBuilder就像CriteriaQuery 的工厂一样。
- CriteriaQuery对象必须在实体类型或嵌入式类型上的Criteria 查询上起作用。
- Employee实体的 CriteriaQuery 对象以下面的方式创建：

```java
CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
```

### ROOT

- Root 定义查询的From子句中能出现的类型

- Criteria查询的查询根定义了实体类型，能为将来导航获得想要的结果，**它与SQL查询中的FROM子句类似。**

- Root实例也是类型化的，且定义了查询的FROM子句中能够出现的类型。

- 查询根实例能通过传入一个实体类型给 AbstractQuery.from方法获得。

- Criteria查询，可以有多个查询根。

- Employee实体的查询根对象可以用以下的语法获得

  ```java
  Root<Employee> employee = criteriaQuery.from(Employee.class);
  ```

### Predicate 过滤条件

- 过滤条件应用到SQL语句的FROM子句中。
- 在criteria 查询中，查询条件通过Predicate 或Expression 实例应用到CriteriaQuery 对象上。
- 这些条件使用 CriteriaQuery .where 方法应用到CriteriaQuery 对象上。
- Predicate 实例也可以用Expression 实例的 isNull， isNotNull 和 in方法获得，复合的Predicate 语句可以使用CriteriaBuilder的and, or andnot 方法构建。
- CriteriaBuilder 也是作为Predicate 实例的工厂，Predicate 对象通过调用CriteriaBuilder 的条件方法（ equal，notEqual， gt， ge，lt， le，between，like等）创建。
- 这些条件使用 CriteriaQuery .where 方法应用到CriteriaQuery 对象上。
- 下面的代码片段展示了Predicate 实例检查年龄大于24岁的员工实例:

```java
Predicate condition = criteriaBuilder.gt(employee.get(Employee_.age), 24); criteriaQuery.where(condition);
```

#### Predicate[] 多个过滤条件

```java
List<Predicate> predicatesList = new ArrayList<Predicate>(); 

predicatesList.add(……Pridicate….)

criteriaQuery.where(predicatesList.toArray(new Predicate[predicatesList.size()]));
```



## 执行sql的几种方法

### 1、通过 EntityManager createQuery

- tableName 实体名
- field 使用 属性名
- 使用位置来设置参数
  - “=？1”
  - query.setParameter(1, userId);

```java
public Result getByEm(int userId){
    String tablename = "User";
    String filed = "userId";
    String sql = "from " + tablename + " u WHERE u." + filed + "=?1";
    Query query = entityManager.createQuery(sql);
    query.setParameter(1, userId);
    entityManager.close();
    return new Result(query.getSingleResult());
}
```

### 2、通过 session createQuery

```java
public Result getBySession(int userId) {
    String tablename = "User";
    String filed = "userId";
    String sql = "from " + tablename + " u WHERE u." + filed + "=?1";
    System.out.println(sql + "--------sql语句-------------");
    Session session = getSession();
    Query users = session.createQuery(sql, User.class);
    users.setParameter(1, userId);
    return new Result(users.getSingleResult());
}
```

### 3、通过 CriteriaBuilder

```java
public Object getBySinglePropertyAndValue(String property, int value, Class<?> clazz) {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(clazz);
    //指明使用那个对象对应的表
    Root<?> root = criteriaQuery.from(clazz);
    //构造where字句的筛选条件
    //这里可以进行自定义的加强
    💚Predicate predicate = criteriaBuilder.equal(root.get(property), value);
    //select  from root构造的表
    criteriaQuery.select(root);
    //where predicate构造的条件
    criteriaQuery.where(predicate);
    Query query = getSession().createQuery(criteriaQuery);
    return query.getSingleResult();
}
```

### 4、加强的Predicate的构造

1、Predicate 的构造是通过criteriaBuilder进行的

```
Predicate predicate = criteriaBuilder.equal(root.get(property), value);
```

2、通过自定义枚举，来进行标识符与predicate的对应构造

```java
//构造枚举类
public enum Comparator {
    EQ,
    NE,// <>
    GT,// >
    GE,// >=
    LT,// <
    LE,// <=
    IS_NULL, //is null
    IS_NOT_NULL, //is not null
    LIKE, //like
    IN, // in
    IS_EMPTY,
    IS_NOT_EMPTY
}
```

3、条件与枚举匹配

```java
switch (Comparator) {
    case EQ:
        predicate = builder.equal(root.get(draftConditionDo.getProperty()), draftConditionDo.getValue());
        break;
    case GE:
        if (draftConditionDo.getValue() instanceof Number) {
            predicate = builder.ge(root.get(draftConditionDo.getProperty()), (Number) draftConditionDo.getValue());
        }
        break;
} 
```

4、使用Predicate[] 构造多个过滤条件

```java
private Predicate genPredicate（...）
```

```java
List<Predicate> predicateAnd = new ArrayList<Predicate>();
List<Predicate> predicatesOr = new ArrayList<Predicate>();
for(){
    //使用3中的匹配器
    //循环构造并ADD()
}
Predicate predicate = null;
if (!predicateAnd.isEmpty() && predicatesOr.isEmpty()) {
	predicate = builder.and(predicateAnd.toArray(new Predicate[predicateAnd.size()]));
}
if (predicateAnd.isEmpty() && !predicatesOr.isEmpty()) {
	predicate = builder.or(predicatesOr.toArray(new Predicate[predicatesOr.size()]));
}
if (!predicateAnd.isEmpty() && !predicatesOr.isEmpty()) {
	predicate = builder.and(
		builder.and(predicateAnd.toArray(new Predicate[predicateAnd.size()])),
		builder.or(predicatesOr.toArray(new Predicate[predicatesOr.size()]))
	);
}
//返回构造的多个条件
return predicate;
```

5、执行顺序

构造条件

```java
public Result getBySinglePropertyAndValue(int userId) {
        ConditionDo conditionDo = new ConditionDo(User.class);
        conditionDo.addAnd(Comparator.EQ,"userId",userId);
        User u = (User) getByCondition(conditionDo);
        return new Result(u);
    }    
```

条件执行

```java
public Object getByCondition(ConditionDo conditionDo) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        Class clazz = conditionDo.getClassType();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<?> root = criteriaQuery.from(clazz);
        Predicate predicate = genPredicate(conditionDo, criteriaBuilder, root);
        criteriaQuery.select(root);
        criteriaQuery.where(predicate);
        Query query = getSession().createQuery(criteriaQuery);
        return query.getSingleResult();
    }
```

6、条件类ConditionDo的原理

```java
//维护一个条件列表
private List<ConditionUnitDo> andCondition;

//条件实体由 匹配器类型、表字段对应属性名、字段值三者组成
public class ConditionUnitDo {
    private Comparator comparator;
    private String property;
    private Object value;
}
```

注意：

这里的条件列表的维护与CriteriaBuilder相关

```java
builder.and()
builder.or()

不同类型的条件就需要维护到不同的列表中

//与条件
private List<ConditionUnitDo> andCondition;
//或条件
private List<ConditionUnitDo> orCondition;
```

7、需要单独再看看的

- orderBy

  ```java
  HashMap<String, FeadSort> feadSortMap = conditionDo.getOrderBy();
  if (feadSortMap != null && !feadSortMap.isEmpty()) {
  	List<Order> orderList = new ArrayList<Order>();
  	Set<String> keys = feadSortMap.keySet();
  	for (String key : keys) {
  		Path path = root.get(key);
  		orderList.add(feadSortMap.get(key) == FeadSort.ASC ? builder.asc(path) : 	builder.desc(path));
  	}
  	criteriaQuery.orderBy(orderList);
  }
  ```

- groupBy

  ```java
  if (!FeadUtil.isEmpty(draftConditionDo.getGroupBy())) {
      List<Path> paths = new ArrayList<>();
      for (String prop : draftConditionDo.getGroupBy()) {
          paths.add(root.get(prop));
      }
      criteriaQuery.groupBy(paths);
  }
  ```

- 分页

  ```java
  Query query = session.createQuery(criteriaQuery);
  query.setFirstResult(begin);
  query.setMaxResults(pageSize);
  return (List<?>) query.getResultList();
  ```

# Hibernate

## Springboot如何获取hibernate Session

1、在Application.propertities中加入


```properties
spring.jpa.properties.hibernate.current_session_context_class=org.springframework.orm.hibernate5.SpringSessionContext
```
本地试了一下，不加这一句也可以。还不知道加和不加有没有区别。
2、在类中通过EntityManager来获取session

```java
public class BaseDao {

    @PersistenceContext
    protected EntityManager entityManager;

    public Session getSession() {
        return entityManager.unwrap(Session.class);
    }
    
}
```

3、用完之后如何关闭

可以关闭em或者session





















