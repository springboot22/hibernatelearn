package com.example.demo;

import com.example.demo.entity.User;
import java.lang.annotation.Annotation;
import javax.persistence.Table;
import org.junit.jupiter.api.Test;

//@SpringBootTest
class DemoApplicationTests {

    @Test
    void contextLoads() {
        Annotation[] as = User.class.getAnnotations();
//        Arrays.stream(as).iterator().forEachRemaining(e ->
//                System.out.println(e.annotationType() + "++" + e.getClass().getAnnotation())
//        );
        Table table = User.class.getAnnotation(Table.class);
        System.out.println(table.name());

    }

}
