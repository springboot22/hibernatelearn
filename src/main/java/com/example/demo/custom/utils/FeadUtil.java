package com.example.demo.custom.utils;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.persistence.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author will.tuo
 * @date 2021/10/14 14:11
 */
public class FeadUtil {

    private static final Logger logger = LogManager.getLogger(FeadUtil.class);

    public static String getTableNameByClass(Class<?> classType) {
        Table table = classType.getAnnotation(Table.class);
        if (table == null) {
            return null;
        }
        return table.name();
    }

    public static boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public static boolean isEmpty(String[] string) {
        return string == null || string.length == 0;
    }

    public static boolean isEmpty(List<?> list) {
        if (list == null) {
            return true;
        }
        if (list.isEmpty()) {
            return true;
        }
        return false;
    }


    public static Object callMethodByName(String methodName, Object o) {
        try {
            Method method = o.getClass().getMethod(methodName, new Class[]{});
            Object value = method.invoke(o, new Object[]{});
            return value;
        } catch (Exception e) {
            return null;
        }
    }


    public static String getFormatDateHourString(Long timeStamp) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(timeStamp);
    }


}
