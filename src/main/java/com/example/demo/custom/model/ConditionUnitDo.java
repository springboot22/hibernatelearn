package com.example.demo.custom.model;


import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/10/14 9:55
 */
@Data
public class ConditionUnitDo {

    public ConditionUnitDo() {
        System.out.println("nmd");
    }

    public ConditionUnitDo(Comparator comparator, String property, Object value) {
        this.comparator = comparator;
        this.property = property;
        this.value = value;
    }

    private Comparator comparator;
    private String property;
    private Object value;
}
