package com.example.demo.custom.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author will.tuo
 * @date 2021/10/14 11:18
 */
public class ConditionComparator {


    private static final Logger logger = LogManager.getLogger(ConditionComparator.class);


    public static Predicate parseCondition(ConditionUnitDo draftConditionDo, CriteriaBuilder builder,
            Root<?> root) {
        if (draftConditionDo == null) {
            return null;
        }
        Predicate predicate = null;
        switch (draftConditionDo.getComparator()) {
            case EQ:
                predicate = builder.equal(root.get(draftConditionDo.getProperty()), draftConditionDo.getValue());
                break;
            case GE:
                if (draftConditionDo.getValue() instanceof Number) {
                    predicate = builder
                            .ge(root.get(draftConditionDo.getProperty()), (Number) draftConditionDo.getValue());
                }
                break;
            case GT:
                if (draftConditionDo.getValue() instanceof Number) {
                    predicate = builder
                            .gt(root.get(draftConditionDo.getProperty()), (Number) draftConditionDo.getValue());
                }
                break;
            case IN:
                CriteriaBuilder.In<Object> in = builder.in(root.get(draftConditionDo.getProperty()));
                Object value = draftConditionDo.getValue();
                if (value != null) {
                    if (value instanceof Array) {
                        Array[] values = (Array[]) value;
                        if (values.length > 0) {
                            for (Object v : values) {
                                in.value(v);
                            }
                        }
                    } else if ((value instanceof List) || (value instanceof ArrayList)) {
                        ArrayList<Object> values = (ArrayList<Object>) value;
                        if (values.size() > 0) {
                            for (Object v : values) {
                                in.value(v);
                            }
                        }
                    } else if (value.getClass().isArray()) {
                        Object[] values = (Object[]) value;
                        if (values.length > 0) {
                            for (Object v : values) {
                                in.value(v);
                            }
                        }
                    } else {
                        logger.error("[DraftData:parseCondition] Condition Error:" + value);
                    }
                } else {
                    in = null;
                }
                predicate = in;
                break;
            case LE:
                if (draftConditionDo.getValue() instanceof Number) {
                    predicate = builder
                            .le(root.get(draftConditionDo.getProperty()), (Number) draftConditionDo.getValue());
                }
                break;
            case LT:
                if (draftConditionDo.getValue() instanceof Number) {
                    predicate = builder
                            .lt(root.get(draftConditionDo.getProperty()), (Number) draftConditionDo.getValue());
                }
                break;
            case NE:
                predicate = builder.notEqual(root.get(draftConditionDo.getProperty()), draftConditionDo.getValue());
                break;
            case LIKE:
                if (draftConditionDo.getValue() instanceof String) {
                    predicate = builder
                            .like(builder.lower(root.get(draftConditionDo.getProperty())),
                                    (String) ((String) draftConditionDo.getValue()).toLowerCase());
                }
                break;
            case IS_NULL:
                predicate = builder.isNull(root.get(draftConditionDo.getProperty()));
                break;
            case IS_NOT_NULL:
                predicate = builder.isNotNull(root.get(draftConditionDo.getProperty()));
                break;
            case IS_EMPTY:
                predicate = builder.isEmpty(root.get(draftConditionDo.getProperty()));
                break;
            case IS_NOT_EMPTY:
                predicate = builder.isNotEmpty(root.get(draftConditionDo.getProperty()));
                break;
            default:
                break;
        }
        if (predicate == null) {
            throw new IllegalArgumentException("filed type error! Property:" + draftConditionDo.getProperty());
        }
        return predicate;
    }

}
