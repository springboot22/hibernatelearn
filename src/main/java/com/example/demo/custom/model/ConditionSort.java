package com.example.demo.custom.model;

/**
 * @author will.tuo
 * @date 2021/10/14 9:57
 */
public enum ConditionSort {
    /**
     * 升降序
     */
    ASC,
    DESC
}
