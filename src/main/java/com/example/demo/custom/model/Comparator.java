package com.example.demo.custom.model;

/**
 * @author will.tuo
 * @date 2021/10/14 9:56
 */
public enum Comparator {
    /**
     * =
     */
    EQ,
    NE,// <>
    GT,// >
    GE,// >=
    LT,// <
    LE,// <=
    IS_NULL, //is null
    IS_NOT_NULL, //is not null
    LIKE, //like
    IN, // in
    IS_EMPTY,
    IS_NOT_EMPTY

}

