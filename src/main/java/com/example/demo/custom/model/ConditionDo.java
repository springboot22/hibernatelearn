package com.example.demo.custom.model;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author will.tuo
 * @date 2021/10/14 9:55
 */

public class ConditionDo implements Serializable {

    private static final long serialVersionUID = 0L;

    public ConditionDo() {
    }

    public ConditionDo(Class<?> classType) {
        this.classType = classType;
    }

    public ConditionDo(Class<?> classType, String flag) {
        this.classType = classType;
    }

    public ConditionDo addAnd(Comparator comparator, String property, Object value) {
        if (Comparator.IN == comparator) {
            if (value == null) {
                throw new IllegalArgumentException("Empty value in Comparator.IN");
            }
            if (value instanceof Array) {
                Array[] values = (Array[]) value;
                if (values.length == 0) {
                    throw new IllegalArgumentException("Empty value in Comparator.IN");
                }

            } else if (value instanceof ArrayList || value instanceof List) {
                ArrayList<Object> values = (ArrayList<Object>) value;
                if (values.size() == 0) {
                    throw new IllegalArgumentException("Empty value in Comparator.IN");
                }
            } else if (value.getClass().isArray()) {
                Object[] values = (Object[]) value;
                if (values.length == 0) {
                    throw new IllegalArgumentException("Empty value in Comparator.IN");
                }
            }
        }
        if (andCondition == null) {
            andCondition = new ArrayList<ConditionUnitDo>();
        }
        andCondition.add(new ConditionUnitDo(comparator, property, value));
        return this;
    }

    public ConditionDo addOr(Comparator comparator, String property, Object value) {
        if (orCondition == null) {
            orCondition = new ArrayList<ConditionUnitDo>();
        }
        orCondition.add(new ConditionUnitDo(comparator, property, value));
        return this;
    }

    public ConditionDo orderBy(String property, ConditionSort conditionSort) {
        if (orderBy == null) {
            orderBy = new HashMap<String, ConditionSort>();
        }
        orderBy.put(property, conditionSort);
        return this;
    }

    public ConditionDo groupBy(String property) {
        if (groupBy == null) {
            groupBy = new ArrayList<String>();
        }
        groupBy.add(property);
        return this;
    }

    public Class getClassType() {
        return classType;
    }

    public void setClassType(Class<?> classType) {
        this.classType = classType;
    }

    public List<ConditionUnitDo> getAndCondition() {
        return andCondition;
    }

    public List<ConditionUnitDo> getOrCondition() {
        return orCondition;
    }

    public HashMap<String, ConditionSort> getOrderBy() {
        return orderBy;
    }

    public List<String> getGroupBy() {
        return groupBy;
    }


    private Class classType;

    private List<String> groupBy;  //分组统计字段

    private HashMap<String, ConditionSort> orderBy;

    private List<ConditionUnitDo> andCondition;

    private List<ConditionUnitDo> orCondition;

}

