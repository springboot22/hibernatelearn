package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.example.demo.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2021/10/13 15:25
 */
@RestController
public class H1Controller {

    @Autowired
    UserService userService;

    /**
     * 保存user
     *
     * @param user
     * @return
     */
    @PostMapping("/saveUser")
    public Result saveUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    /**
     * 查询所有user
     *
     * @return
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return userService.queryAll();
    }

    @GetMapping("/queryAll1")
    public Result queryAll1(int userId) {
//        return userService.getBySinglePropertyAndValue(userId);
        User user = new User();
        user.setUserId(8);
        return userService.getByEm(userId);
    }

    /**
     * @return
     */
    @GetMapping("/getByNameAndAge")
    public Result getByNameAndAge(int userId) {
        return userService.getById(userId);
    }

}
