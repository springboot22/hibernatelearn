package com.example.demo.dao;

import com.example.demo.custom.model.ConditionComparator;
import com.example.demo.custom.model.ConditionDo;
import com.example.demo.custom.model.ConditionSort;
import com.example.demo.custom.model.ConditionUnitDo;
import com.example.demo.custom.utils.FeadUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.Session;

/**
 * @author will.tuo
 * @date 2021/10/14 8:57
 */
public class BaseDao {

    @PersistenceContext
    protected EntityManager entityManager;

    public Session getSession() {
        return entityManager.unwrap(Session.class);
    }

    public Object getBySinglePropertyAndValue(String property, int value, Class<?> clazz) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<?> root = criteriaQuery.from(clazz);
        Predicate predicate = criteriaBuilder.equal(root.get(property), value);
        criteriaQuery.select(root);
        criteriaQuery.where(predicate);
        Query query = getSession().createQuery(criteriaQuery);
        return query.getSingleResult();
    }


    public Object getByCondition(ConditionDo conditionDo) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        Class clazz = conditionDo.getClassType();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<?> root = criteriaQuery.from(clazz);
        Predicate predicate = genPredicate(conditionDo, criteriaBuilder, root);
        criteriaQuery.select(root);
        criteriaQuery.where(predicate);
        Query query = getSession().createQuery(criteriaQuery);
        return query.getSingleResult();
    }

    /***
     * 分页读取多条记录
     *
     * @param draftConditionDo 查询条件
     * @param pageIndex  页码  0:不分页
     * @param pageSize  每页大小 0:不分页
     *
     * ***/
    public List<?> queryListByPage(ConditionDo draftConditionDo, int pageIndex, int pageSize) {
        return queryListByIndex(draftConditionDo, (pageIndex - 1) * pageSize, pageSize);
    }

    /***
     * 分页读取多条记录
     *
     * @param draftConditionDo 查询条件
     * @param begin  页码  0:不分页
     * @param pageSize  每页大小 0:不分页
     *
     * ***/
    public List<?> queryListByIndex(ConditionDo draftConditionDo, int begin, int pageSize) {
        Session session = getSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = builder.createQuery(draftConditionDo.getClassType());
        Root<?> root = criteriaQuery.from(draftConditionDo.getClassType());
        Predicate predicate = genPredicate(draftConditionDo, builder, root);
        criteriaQuery.select(root);
        if (predicate != null) {
            criteriaQuery.where(predicate);
        }
        HashMap<String, ConditionSort> feadSortMap = draftConditionDo.getOrderBy();
        if (feadSortMap != null && !feadSortMap.isEmpty()) {
            List<Order> orderList = new ArrayList<Order>();
            Set<String> keys = feadSortMap.keySet();
            for (String key : keys) {
                Path path = root.get(key);
                orderList.add(feadSortMap.get(key) == ConditionSort.ASC ? builder.asc(path) : builder.desc(path));
            }
            criteriaQuery.orderBy(orderList);
        }

        if (!FeadUtil.isEmpty(draftConditionDo.getGroupBy())) {
            List<Path> paths = new ArrayList<>();
            for (String prop : draftConditionDo.getGroupBy()) {
                paths.add(root.get(prop));
            }
            criteriaQuery.groupBy(paths);
        }

        Query query = session.createQuery(criteriaQuery);
        query.setFirstResult(begin);
        query.setMaxResults(pageSize);
        //注释测试
        return (List<?>) query.getResultList();
    }
    
    private Predicate genPredicate(final ConditionDo draftConditionDo, CriteriaBuilder builder, Root<?> root) {
        List<Predicate> predicateAnd = new ArrayList<>();
        List<Predicate> predicatesOr = new ArrayList<>();

        List<ConditionUnitDo> draftConditionDoAnds = draftConditionDo.getAndCondition();
        if (draftConditionDoAnds != null) {
            for (ConditionUnitDo conditionUnitDo : draftConditionDoAnds) {
                Predicate predicate = ConditionComparator.parseCondition(conditionUnitDo, builder, root);
                if (predicate != null) {
                    predicateAnd.add(predicate);
                }
            }
        }

        List<ConditionUnitDo> draftParamsOr = draftConditionDo.getOrCondition();
        if (draftParamsOr != null) {
            for (ConditionUnitDo conditionUnitDo : draftParamsOr) {
                Predicate predicate = ConditionComparator.parseCondition(conditionUnitDo, builder, root);
                if (predicate != null) {
                    predicatesOr.add(predicate);
                }
            }
        }

        Predicate predicate = null;
        if (!predicateAnd.isEmpty() && predicatesOr.isEmpty()) {
            predicate = builder.and(predicateAnd.toArray(new Predicate[predicateAnd.size()]));
        }
        if (predicateAnd.isEmpty() && !predicatesOr.isEmpty()) {
            predicate = builder.or(predicatesOr.toArray(new Predicate[predicatesOr.size()]));
        }
        if (!predicateAnd.isEmpty() && !predicatesOr.isEmpty()) {
            predicate = builder.and(
                    builder.and(predicateAnd.toArray(new Predicate[predicateAnd.size()])),
                    builder.or(predicatesOr.toArray(new Predicate[predicatesOr.size()]))
            );
        }
        return predicate;
    }

}
