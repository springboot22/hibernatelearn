package com.example.demo.service.impl;

import com.example.demo.custom.model.Comparator;
import com.example.demo.custom.model.ConditionDo;
import com.example.demo.dao.BaseDao;
import com.example.demo.dao.UserRepository;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.example.demo.utils.Result;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/10/13 15:50
 */
@Service
public class UserServiceImpl extends BaseDao implements UserService {

    @Resource
    UserRepository userRepository;


    @Override
    public Result saveUser(User user) {
        User save = userRepository.save(user);
        return new Result(save);
    }

    @Override
    public Result queryAll() {
        List<User> list = userRepository.findAll();
        return new Result(list);
    }

    @Override
    public Result getById(int id) {
        List<User> list = userRepository.getByUserId(id);
        return new Result(list);
    }

    @Override
    public Result getBySession(int userId) {
        String tablename = "User";
        String filed = "userId";
        String sql = "from " + tablename + " u WHERE u." + filed + "=?1";
        System.out.println(sql + "--------sql语句-------------");
        Session session = getSession();
        Query users = session.createQuery(sql, User.class);
        users.setParameter(1, userId);
        return new Result(users.getSingleResult());
    }

    @Override
    public Result getByEm(int userId) {
        String tablename = "User";
        String filed = "userId";
        String sql = "from " + tablename + " u WHERE u." + filed + "=?1";
        Query query = entityManager.createQuery(sql);
        query.setParameter(1, userId);
        entityManager.close();
        return new Result(query.getSingleResult());
    }

    @Override
    public Result getBySinglePropertyAndValue(int userId) {
//        User u = (User) getBySinglePropertyAndValue("userId", userId, User.class);
        ConditionDo conditionDo = new ConditionDo(User.class);
        conditionDo.addAnd(Comparator.EQ, "userId", userId);
        User u = (User) getByCondition(conditionDo);
        return new Result(u);
    }

//    public Result getById1(int userId){
//        FeadConditionDo feadConditionDo = new FeadConditionDo(User.class);
//        feadConditionDo.addAnd(Comparator.EQ,"userId",userId);
//
//    }


}
