package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.utils.Result;

/**
 * @author will.tuo
 * @date 2021/10/13 15:48
 */
public interface UserService {

    /**
     * 保存 user 对象
     *
     * @param user
     * @return
     */
    Result saveUser(User user);

    /**
     * 查询所有user
     *
     * @return
     */
    Result queryAll();

    Result getById(int id);

    Result getBySession(int userId);

    Result getByEm(int userId);

    /**
     * sadf
     *
     * @param userId
     * @return
     */
    Result getBySinglePropertyAndValue(int userId);

}
