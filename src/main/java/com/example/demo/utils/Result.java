package com.example.demo.utils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author will.tuo
 * @date 2021/10/13 15:44
 */
@Setter
@Getter
public class Result {

    private int code = 200;
    private String msg = "操作成功";
    private Object data;

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(Object data) {
        this.data = data;
    }
}
