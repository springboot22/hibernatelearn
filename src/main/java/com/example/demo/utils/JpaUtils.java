package com.example.demo.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author will.tuo
 * @date 2021/10/13 16:51
 */
public class JpaUtils {

    private static EntityManagerFactory entityManagerFactory;

    static {
        //1.加载配置文件，创建entityManagerFactory
        entityManagerFactory = Persistence.createEntityManagerFactory("myJpa");
    }

    public static EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
